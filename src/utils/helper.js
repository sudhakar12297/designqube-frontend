const apiBaseURL = "http://localhost:8080/api";

export const apiCall = async (url = "", method, body = {}) => {
  const options =
    method === "GET"
      ? {
          method,
          headers: {
            "Content-type": "application/json",
          },
        }
      : {
          method,
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify(body),
        };
  const response = await fetch(apiBaseURL + url, options);
  const data = await response.json();
  if (response.ok) return data;
  throw data;
};
