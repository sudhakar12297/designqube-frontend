import { apiCall } from "../../utils/helper";

export const fetchTodos = () => {
  console.log("inside get todo");
  return async (dispatch) => {
    dispatch({ type: "GET_TODOS_START" });
    try {
      const tasks = await apiCall("/todo", "GET");
      console.log(tasks);
      dispatch({ type: "GET_TODOS_SUCCESS", tasks });
    } catch (e) {
      dispatch({ type: "GET_TODOS_FAILURE" });
    }
  };
};

export const postTodo = (body) => {
  return async (dispatch) => {
    dispatch({ type: "POST_TODO_START" });
    try {
      await apiCall(`/todo`, "POST", body);
      dispatch({ type: "POST_TODO_SUCCESS" });
      dispatch(fetchTodos());
    } catch (e) {
      console.log("error", e);
      dispatch({ type: "POST_TODO_FAILURE" });
    }
  };
};

export const deleteTodo = (id) => {
  return async (dispatch) => {
    dispatch({ type: "DELETE_TODO_START" });
    try {
      await apiCall(`/todo/${id}`, "DELETE");
      dispatch({ type: "DELETE_TODO_SUCCESS" });
      dispatch(fetchTodos());
    } catch (e) {
      console.log("error", e);
      dispatch({ type: "DELETE_TODO_FAILURE" });
    }
  };
};
