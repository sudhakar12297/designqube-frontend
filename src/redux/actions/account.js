import { apiCall } from "../../utils/helper";
import Cookies from 'js-cookie';

export const fetchLogin = (body) => {
  return async (dispatch) => {
    dispatch({ type: "LOGIN_START" });
    try {
      const user = await apiCall(`/account/login`, "POST", body);
      Cookies.set('isLoggedIn', true)
      dispatch({ type: "LOGIN_SUCCESS", user });
    } catch (e) {
      console.log("error", e);
      dispatch({ type: "LOGIN_FAILURE" });
    }
  };
};

export const postUser = (body) => {
  console.log(body);
  return async (dispatch) => {
    dispatch({ type: "REGISTER_START" });
    try {
      await apiCall(`/account/register`, "POST", body);
      dispatch({ type: "REGISTER_SUCCESS" });
    } catch (e) {
      console.log("error", e);
      dispatch({ type: "REGISTER_FAILURE" });
    }
  };
};
