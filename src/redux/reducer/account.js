const initialState = {
  isLoggedIn: false,
  user: {},
  userRegistered: false,
  isLoading: true,
  isError: false,
};

const account = (state = initialState, action) => {
  const { type, user } = action;
  switch (type) {
    case "REGISTER_START":
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case "REGISTER_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isError: false,
        userRegistered: true,
      };
    case "REGISTER_FAILURE":
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case "LOGIN_START":
      return {
        ...state,
        isLoading: true,
        isError: false,
        userRegistered: false,
      };
    case "LOGIN_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isError: false,
        isLoggedIn: true,
        user,
      };
    case "LOGIN_FAILURE":
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export const getUser = (state) => state.account.user;
export const getIsLoggedIn = (state) => state.account.isLoggedIn;
export const getIsLoading = (state) => state.account.isLoading;
export const getIsError = (state) => state.account.isError;

export default account;
