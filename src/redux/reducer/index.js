import { combineReducers } from "redux";
import todo from "./todo";
import account from "./account";

const rootReducer = combineReducers({
  todo,
  account
});

export default rootReducer;
