const initialState = {
  tasks: [],
  isLoading: true,
  isError: false,
};

const todo = (state = initialState, action) => {
  const { type, tasks } = action;
  switch (type) {
    case "GET_TODOS_START":
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case "GET_TODOS_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isError: false,
        tasks,
      };
    case "GET_TODOS_FAILURE":
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export const getTasks = (state) => state.todo.tasks;
export const getIsLoading = (state) => state.todo.isLoading;
export const getIsError = (state) => state.todo.isError;

export default todo;
