import React, { useState } from "react";

const TodoInput = (props) => {
  const { create } = props;
  const [task, setTask] = useState("");

  const onChange = (e) => {
    setTask(e.target.value);
  };

  const onkeypress = (e) => {
    if (e.which === 13) {
      create(task);
    }
  };

  return (
    <div>
      <input
        type="text"
        value={task}
        onChange={onChange}
        placeholder="Enter task"
        onKeyPress={onkeypress}
      />
    </div>
  );
};

export default TodoInput;
