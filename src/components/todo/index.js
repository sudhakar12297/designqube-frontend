import React from "react";

const Todo = (props) => {
  const { tasks = [], deleteTask } = props;
  return (
    <div>
      {tasks?.map((task) => (
        <div>
          <p>{task.name}</p>
          <button onClick={() => deleteTask(task._id)}>delete</button>
        </div>
      ))}
    </div>
  );
};

export default Todo;
