import React, { useState } from "react";

const Account = (props) => {
  const { isLogin = true, submit } = props;
  const [userDetails, setUserDetails] = useState({ email: "", password: "" });
  const [err, setErr] = useState({ isInvalidEmail: false, msg: "" });

  const onChange = (e) => {
    if (e.target.name === "email") {
      const emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      if (emailRegex.test(e.target.value)) {
        setErr({ ...err, isInvalidEmail: false });
      } else {
        setErr({ ...err, isInvalidEmail: true });
      }
    }
    setErr({ ...err, msg: "" });
    setUserDetails({ ...userDetails, [e.target.name]: e.target.value });
  };

  const onSubmit = () => {
    if (
      userDetails.email.length &&
      !err.isInvalidEmail &&
      userDetails.password.length
    ) {
      submit(isLogin, userDetails);
    } else {
      setErr({ ...err, msg: "Please enter all fields" });
    }
  };

  return (
    <div>
      <input
        type="email"
        name="email"
        value={userDetails.email}
        onChange={onChange}
        placeholder="Enter email"
      />
      {err.isInvalidEmail && <p style={{color:'red'}}>Invalid email address</p>}
      <input
        type="password"
        name="password"
        value={userDetails.password}
        onChange={onChange}
        placeholder="Enter password"
      />
      {err.msg && <p style={{color:'red'}}>{err.msg}</p>}
      <button onClick={onSubmit}>{isLogin ? "Login" : "Register"}</button>
    </div>
  );
};

export default Account;
