import React from "react";
import { useDispatch, useSelector } from "react-redux";
import AccountComp from "../../components/account";
import { fetchLogin, postUser } from "../../redux/actions/account";
import { getIsLoggedIn } from "../../redux/reducer/account";
import { useHistory } from "react-router-dom";
import Cookies from "js-cookie";

const Account = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const isLoggedIn = Cookies.get("isLoggedIn");
  const onSubmit = (isLogin, user) => {
    isLogin ? dispatch(fetchLogin(user)) : dispatch(postUser(user));
  };

  return (
    <div>
      {isLoggedIn && history.push("/todo")}
      <AccountComp isLogin={props.isLogin} submit={onSubmit} />
    </div>
  );
};

export default Account;
