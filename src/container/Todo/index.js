import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import TaskInput from "../../components/todo/todo-input";
import DisplayTasks from "../../components/todo";
import { fetchTodos, postTodo, deleteTodo } from "../../redux/actions/todo";
import { getTasks } from "../../redux/reducer/todo";
const Home = () => {
  const dispatch = useDispatch();

  const tasks = useSelector(getTasks);

  useEffect(() => {
    console.log('1111');
    dispatch(fetchTodos());
  }, []);

  const create = (task) => {
    console.log(task);
    dispatch(postTodo({ name: task }));
  };

  const remove = (id) => {
    dispatch(deleteTodo(id))
  };

  return (
    <>
      <TaskInput create={create} />
      <DisplayTasks tasks={tasks} deleteTask={remove} />
    </>
  );
};

export default Home;
