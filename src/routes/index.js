import React from "react";
import { useSelector } from "react-redux";
import { Switch, Route, BrowserRouter, Redirect } from "react-router-dom";
import Todo from "../container/Todo";
import Account from "../container/Account";
import { getIsLoggedIn } from "../redux/reducer/account";
import Cookies from 'js-cookie';

const Router = () => {
  const isLoggedIn = Cookies.get('isLoggedIn')

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/login">
          <Account isLogin={true} />
        </Route>
        <Route exact path="/register">
          <Account isLogin={false} />
        </Route>
        <PrivateRoute isLoggedIn={isLoggedIn} path="/todo" component={Todo} />
      </Switch>
    </BrowserRouter>
  );
};

const PrivateRoute = (props) => {
  const { isLoggedIn, path, component } = props;
  console.log(isLoggedIn);
  if (isLoggedIn) {
    return <Route exact path={path} component={component} />;
  }
  return <Redirect to="/login" />;
};

export default Router;
